package com.aboalata.weatherjetpackcomose.repository

import com.aboalata.weatherjetpackcomose.data.CityWeather
import com.aboalata.weatherjetpackcomose.data.WeatherApi
import com.aboalata.weatherjetpackcomose.util.Resource
import javax.inject.Inject

class WeatherRepository @Inject constructor(private val weatherApi: WeatherApi) {

    suspend fun getWeatherForCity(city: String): Resource<CityWeather> {

        return try {
            val response = weatherApi.getWeatherForCity(city)
            if (response.isSuccessful) {
                Resource.success(response.body())
            } else {
                Resource.error(response.message(), null)
            }
        } catch (e: Exception) {
            Resource.error("Check Your Internet", data = null)
        }
    }
}