package com.aboalata.weatherjetpackcomose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import com.aboalata.weatherjetpackcomose.ui.theme.WeatherJetpackComoseTheme
import com.aboalata.weatherjetpackcomose.ui.util.Navigation
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WeatherJetpackComoseTheme {

                Surface(color = MaterialTheme.colors.background) {
                    Box(
                        modifier = Modifier.background(

                            brush = Brush.linearGradient(
                                colors = listOf(
                                    MaterialTheme.colors.primary,
                                    MaterialTheme.colors.primaryVariant
                                )
                            )
                        )
                    ) {

                        Navigation()

                    }

                }
            }
        }
    }
}

