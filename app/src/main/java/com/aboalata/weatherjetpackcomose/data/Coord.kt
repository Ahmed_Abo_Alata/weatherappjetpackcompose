package com.aboalata.weatherjetpackcomose.data

data class Coord(
    val lat: Double,
    val lon: Double
)