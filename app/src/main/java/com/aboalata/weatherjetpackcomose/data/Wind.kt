package com.aboalata.weatherjetpackcomose.data

data class Wind(
    val deg: Int,
    val gust: Double,
    val speed: Double
)