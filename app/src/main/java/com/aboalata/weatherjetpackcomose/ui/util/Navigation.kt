package com.aboalata.weatherjetpackcomose.ui.util

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.aboalata.weatherjetpackcomose.ui.choosecity.ChooseCityScreen
import com.aboalata.weatherjetpackcomose.ui.weathercity.WeatherCity


@Composable
fun Navigation() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Screen.ChooseCityScreen.route)
    {
        composable(Screen.ChooseCityScreen.route)
        {

            ChooseCityScreen(navController = navController)
        }

        composable(Screen.WeatherCityScreen.route + "{city}")
        { navBackStackEntry ->
            val city = navBackStackEntry.arguments?.getString("city") ?: " Cairo"
            WeatherCity(city = city)

        }
    }
}