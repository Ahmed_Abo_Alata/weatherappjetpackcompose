package com.aboalata.weatherjetpackcomose.ui.weathercity

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aboalata.weatherjetpackcomose.data.CityWeather
import com.aboalata.weatherjetpackcomose.repository.WeatherRepository
import com.aboalata.weatherjetpackcomose.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class WeatherViewModel @Inject constructor(private val weatherRepository: WeatherRepository) :ViewModel() {

    private val _cityWeather= mutableStateOf<Resource<CityWeather>>(Resource.loading(data = null))

    val cityWeather:State<Resource<CityWeather>> = _cityWeather

    fun getWeatherForCity( city:String)
    {
        viewModelScope.launch {

            _cityWeather.value= weatherRepository.getWeatherForCity(city)
        }
    }
}