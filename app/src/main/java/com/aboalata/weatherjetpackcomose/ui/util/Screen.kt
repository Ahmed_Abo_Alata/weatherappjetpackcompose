package com.aboalata.weatherjetpackcomose.ui.util

sealed class Screen(val route: String) {
    object ChooseCityScreen : Screen("choose_cityScreen")
    object WeatherCityScreen : Screen("weather_cityScreen/")
}
